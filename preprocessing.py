from datetime import datetime
import warnings
import pandas as pd
from logs import LoggerFactory
logger = LoggerFactory.get_logger("Notifying",log_level ="INFO")


def preprocess_data(df):
    warnings.filterwarnings("ignore")
    # extract last 12 months data .. latest data
    current_date = datetime.today().date()
    n = 18
    past_time = current_date - pd.DateOffset(months=n)
    past_date = past_time.date()
    print("past date is: ",past_date)
    df = df[df['ORDER_DATE'].dt.date > past_date]

    df = df.drop_duplicates()
    df.dropna(subset=['PART_ID'], inplace=True)
    df['BUYER'] = df['BUYER'].str.lower()
    # replace buyers :- PMK,ADMIN,PLANNING,BUYER2,SALES,SYSADM
    patterns = [('pmk', 'other'), ('admin', 'other'), ('purch', 'other'), ('sysadm', 'other')]
    df['BUYER'] = generalize(df['BUYER'], patterns)
    df['Total Spend'] = df['ORDER_QTY'] * df['UNIT_PRICE']
    df = df[df['UNIT_PRICE'] > 0]
    original_df = df.copy()

    # C:- completed orders, dont take into account because they are already completed
    # X :- cancelled orders
    df_Firmed_status = original_df[original_df['STATUS'] != 'C']
    df_Firmed_status = df_Firmed_status[df_Firmed_status['STATUS'] != 'X']
    # firmed orders are open orders
    df_Firmed_status = df_Firmed_status[df_Firmed_status['STATUS'] == 'F']
    return df_Firmed_status, original_df

def preprocess_parts_site(parts_df):
    current_date = datetime.today().date()
    n = 12
    past_time = current_date - pd.DateOffset(months=n)
    past_date = past_time.date()
    parts_df = parts_df[parts_df['CREATE_DATE'].dt.date > past_date]
    # drop duplicate rows
    parts_df = parts_df.drop_duplicates()
    print("columns are:",parts_df.columns)
    # REMOVE UNIT_MATERIAL_COST WITH 0
    parts_df = parts_df[parts_df['UNIT_MATERIAL_COST'] != 0]
    # REMOVE ROWS WITH PREF_VENDOR_ID NAN
    parts_df.dropna(subset=['PREF_VENDOR_ID'], inplace=True)
    return parts_df

def generalize(data_column_series, match_name, default=None, regex=False, case=False):
    """ Search a series for text matches.
        Based on code from https://www.metasnake.com/blog/pydata-assign.html


    data_column_series: pandas series to search
    match_name: tuple containing text to search for
    default: If no match, use this to provide a default value, otherwise use the original text
    regex: Boolean to indicate if match_name contains a  regular expression
    case: Case sensitive search

    Returns a pandas series with the matched value

    """
    seen = None
    for match, name in match_name:
        mask = data_column_series.str.contains(match, case=case, regex=regex)
        if seen is None:
            seen = mask
        else:
            seen |= mask

        data_column_series = data_column_series.where(~mask, name)
    if default:
        data_column_series = data_column_series.where(seen, default)
    else:
        data_column_series = data_column_series.where(seen, data_column_series.values)
    return data_column_series
