import logging
import logging.config
from logging.handlers import TimedRotatingFileHandler

class LoggerFactory(object):
    _LOG = None

    @staticmethod
    def create_Logger(log_file, log_level):
        '''
        A private method that interacts with the python
        logging module
        '''
        # set the logging format
        log_format = "%(asctime)s:%(levelname)s:%(message)s (%(filename)s:%(lineno)d)"

        # Initialize the class variable with logger object
        LoggerFactory._LOG = logging.getLogger(log_file)
        logging.basicConfig(level=logging.INFO, format=log_format, filename='info.log', datefmt="%Y-%m-%d %H:%M:%S")
        #logging.basicConfig(level=logging.INFO, format=log_format, datefmt="%Y-%m-%d %H:%M:%S")

        # set the logging level based on the user selection
        if log_level == "INFO":
            LoggerFactory._LOG.setLevel(logging.INFO)
        elif log_level == "ERROR":
            LoggerFactory._LOG.setLevel(logging.ERROR)
        elif log_level == "DEBUG":
            LoggerFactory._LOG.setLevel(logging.DEBUG)
        return LoggerFactory._LOG


    @staticmethod
    def get_logger(log_file, log_level):
        '''
        A static method called by other modules to initialize logger in
        their own module
        '''
        # logging.config.fileConfig(fname='logconfig.conf',disable_existing_loggers=False)
        logger = LoggerFactory.create_Logger(log_file,log_level)

        # Get the logger specified in the file
        logging.getLogger(__name__)
        return logger
